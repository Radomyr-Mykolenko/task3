﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;

namespace WebApplication1.Parking
{
    class Parking                               //Клас парковки
    {
        static public float parking_balance; //рахунок/баланс грошей на парковці
        public int vehicles_parked;     //запарковано транспортних засобів

        public Dictionary<string, Vehicle> Parked_vehicles = new Dictionary<string, Vehicle>(); //Словник, де зберігаємо запарковані автомобілі
        static public Dictionary<DateTime, float> ParkingBalance = new Dictionary<DateTime, float>(); // Журнал транзакцій парковки

        static public Dictionary<DateTime, PaymentDetails> Transactions = new Dictionary<DateTime, PaymentDetails>(); // Журнал транзакцій парковки
        static public void ShowTransactions()
        {

            foreach (KeyValuePair<DateTime, PaymentDetails> kvp in Transactions)
            {
                Console.WriteLine(" {0}  vehicle_Id  {1}    amount_spent  {2}   vehicle_type {3}   balance {4}",
                        kvp.Key.ToLongTimeString(), kvp.Value.vehicleId, kvp.Value.amountspent, kvp.Value.vehicletype, kvp.Value.vehicle_bal);
            }

        }

        static public void ShowMoneyReceived()
        {

            foreach (KeyValuePair<DateTime, float> kvp in ParkingBalance)
            {
                Console.WriteLine(" {0}  зароблено парковкою {1} грошей",
                        kvp.Key.ToLongTimeString(), kvp.Value);
            }

        }

        public void SetParkingBalance() //задаємо початковий баланс парковки
        {
            parking_balance = Parking_Settings.initial_balance;
        }

        public void ShowParkingBalance()
        {
            Console.WriteLine($"  Поточний баланс парковки становить {parking_balance} грошей \n");
        }

        public void ShowQuantityVehiclesParked()
        {
            if (vehicles_parked == 0)
            {
                Console.WriteLine("  Парковка повнiстю вiльна, можна паркуватись \n");
                Console.WriteLine($"  Не забувайте, що найбiльше можна поставити {Parking_Settings.parking_capacity} транспортних засобiв \n");
            }
            else
            {
                Console.WriteLine($" На парковцi зайнято {vehicles_parked} мiсць");
                Console.WriteLine($" Доступно {Parking_Settings.parking_capacity - vehicles_parked} вiльних мiсць \n");
            }
        }

        public void ShowListOfVehiclesParked()
        {
            if (vehicles_parked != 0)
            {
                int i = 1;
                foreach (Vehicle c in Parked_vehicles.Values)
                {
                    Console.Write($"  {i} - ");
                    c.Display_Info_about_vehicle();
                    i++;
                }
            }
            else
            {
                Console.WriteLine("  Порожньо на парковцi");
            }
        }

        //public void ShowTransactions()
        //{

        //    foreach (KeyValuePair<string,PaymentDetails> kvp in Transactions)
        //    {
        //        Console.WriteLine("Key = {0}, vehicle_Id = {1}, amount_spent = {2}, balance = {3}",
        //                kvp.Key, kvp.Value.vehicleId, kvp.Value.amountspent, kvp.Value.vehicle_bal);             
        //    }

        //}

        //визначення делегату
        public delegate void BalanceParkingStateHandler(object sender, BalanceParkingEventArgs e);
        //події, які відбуваються, коли поповнюється рахунок парковки, ми лише поповнюємо рахунок парковки за рахунок списаних коштів із транспортних засобів
        public event BalanceParkingStateHandler ToppedUp;

        public event BalanceParkingStateHandler VehicleAdded;

        public event BalanceParkingStateHandler VehicleRemoved;

        public void TopUp(float amount)
        {
            parking_balance += amount;
            //var args = new BalanceParkingEventArgs($"Баланс парковки був поповнений на {amount} грошей", amount);
            //ToppedUp?.Invoke(this, args);
        }

        public void VehicleAdd()
        {
            if (vehicles_parked < Parking_Settings.parking_capacity)        //перевірка, чи не вийшли ми за ліміт парковки
            {
                vehicles_parked += 1;
                var args = new BalanceParkingEventArgs("  На парковку було додано транспортний засiб");
                VehicleAdded?.Invoke(this, args);
            }
            else
            {
                var args = new BalanceParkingEventArgs($"  Заборонено паркуватись, перевищено лiмiт в {Parking_Settings.parking_capacity} транспортних засобiв");
                VehicleRemoved?.Invoke(this, args);
            }
        }

        public void VehicleRemove()
        {
            if (vehicles_parked >= 1)
            {
                vehicles_parked -= 1;
                var args = new BalanceParkingEventArgs("  З парковки прибрали транспортний засiб");
                VehicleRemoved?.Invoke(this, args);
            }
            else
            {
                var args = new BalanceParkingEventArgs("  Вже всi прибранi транспортнi засоби");
                VehicleRemoved?.Invoke(this, args);
            }
        }

    }
    public class BalanceVehicleEventArgs            // клас аргументів подій для балансу транспортного засобу
    {
        public string ActionMessage { get; }

        public float Amount { get; }

        public BalanceVehicleEventArgs(string actionMessage, float amount)
        {
            ActionMessage = actionMessage;
            Amount = amount;
        }
    }

    public class BalanceParkingEventArgs            // клас аргументів подій для балансу парковки
    {
        public string ActionMessage { get; }

        public float Amount { get; }

        public int Quantity { get; }                //кількість авто на парковці

        public BalanceParkingEventArgs(string actionMessage, float amount)
        {
            ActionMessage = actionMessage;
            Amount = amount;
        }
        public BalanceParkingEventArgs(string actionMessage)
        {
            ActionMessage = actionMessage;
            Quantity = 1;
        }
    }
}