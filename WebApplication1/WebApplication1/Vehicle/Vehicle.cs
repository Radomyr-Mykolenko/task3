﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;

class Vehicle
{
    public string vehicle_number;
    public string type_of_vehicle;      //lorry, bus, smallcar, bike
    public float vehicle_balance; //рахунок/баланс грошей на транспортному засобі       
    public float penalty;
    public enum VehicleType
    {
        smallcar,
        lorry,
        bus,
        bike
    }

    public float parking_price;         //сюди буде записуватись вартість паркування Транспортного засобу

    public Vehicle(string number, VehicleType type)
    {


        vehicle_number = number;
        type_of_vehicle = Convert.ToString(type);
        parking_price = 0; //компілятор чомусь попросив спочатку хоч якось ініціалізувати це поле
        switch (type)
        {
            case VehicleType.smallcar:
                parking_price = Parking_Settings.parking_price_for_smallcar;

                break;
            case VehicleType.lorry:
                parking_price = Parking_Settings.parking_price_for_lorry;

                break;
            case VehicleType.bus:
                parking_price = Parking_Settings.parking_price_for_bus;

                break;
            case VehicleType.bike:
                parking_price = Parking_Settings.parking_price_for_bike;

                break;
            default:
                Console.WriteLine($"Не передбачений даною програмою тип авто {type}");
                break;

        }
        vehicle_balance = Parking_Settings.initial_vehicle_balance;

        ToppedUp = null;
        Withdrawn = null;
        SetTimer(Parking_Settings.time_for_parking);
    }



    public void Display_Info_about_vehicle()
    {
        Console.WriteLine($"  Номер ТЗ: {vehicle_number}, тип ТЗ: {type_of_vehicle}, \n плата за паркування для нього становить: {parking_price}, баланс рахунку для оплати паркування: {vehicle_balance}");
        Console.WriteLine();

    }

    //визначення делегату
    public delegate void BalanceVehicleStateHandler(object sender, BalanceVehicleEventArgs e);
    //події, які відбуваються, коли поповнюється рахунок транспортного засобу
    public event BalanceVehicleStateHandler ToppedUp;

    public event BalanceVehicleStateHandler Withdrawn;

    public void TopUp(float amount)
    {
        vehicle_balance += amount;
        var args = new BalanceVehicleEventArgs($"  Баланс транспортного засобу з номером {vehicle_number} був поповнений на {amount} грошей", amount);
        ToppedUp?.Invoke(this, args);
    }

    public void Withdraw(float amount)
    {
        if (vehicle_balance <= 0)
        {
            penalty = Parking_Settings.penalty;
            vehicle_balance -= (amount * penalty);  //застосовуємо штраф за мінусовий баланс
        }
        else
        {
            penalty = 1;
            vehicle_balance -= amount * penalty;
        }
    }

    private static Timer aTimer;

    public void SetTimer(int N) //таймер, який щохвилини записує в файл
    {
        // Create a timer with a two second interval.
        aTimer = new System.Timers.Timer(N);
        // Hook up the Elapsed event for the timer. 
        aTimer.Elapsed += OnTimedEvent;

        aTimer.AutoReset = true;
        aTimer.Enabled = true;
    }

    public void OnTimedEvent(Object source, ElapsedEventArgs e)                  //тут власне і відбуваються транзакції
    {
        Withdraw(parking_price);
        //string timenow = Convert.ToString(DateTime.Now);
        //Parking.Transactions[Convert.ToString(timenow)] = new PaymentDetails() { vehicleId = vehicle_number, amountspent = Parking_Settings.parking_price_for_smallcar, vehicle_bal = vehicle_balance };
        Parking.Transactions[DateTime.Now] = new PaymentDetails() { vehicleId = vehicle_number, amountspent = parking_price, vehicletype = type_of_vehicle, vehicle_bal = vehicle_balance };
        Parking.parking_balance += parking_price;
        Parking.ParkingBalance[DateTime.Now] = parking_price * penalty;
    }

    public void Stoptimer()
    {
        aTimer.Stop();
        aTimer.Dispose();
    }
}