﻿static class Parking_Settings // статичний клас налаштувань парковки
{
    static public float initial_balance = 0; // початковий баланс парковки
    static public int parking_capacity = 10; // максимальна місткість парковки
                                             //періодичність,  з якою будуть зніматись кошти, за замовчуванням це 5 секунд

    //тарифи за паркування
    public const float parking_price_for_smallcar = 2; //легкове авто
    public const float parking_price_for_lorry = 5;        //вантажне авто
    public const float parking_price_for_bus = 3.5f;       //автобус
    public const float parking_price_for_bike = 1;         //мотоцикл

    public const float penalty = 2.5f;         //коефіціент штрафу, який накладається на Транспортний засіб, якщо на його рахунку недостатньо коштів для оплати стоянки. 
                                               //Допоки Транспортний засіб не виплатить гроші Парковці, з цього рахунку списуватимуться кошти з урахуванням цього коефіціенту
    public const float initial_vehicle_balance = 200;

    public const int time_for_parking = 8000; //параметр часу N

}