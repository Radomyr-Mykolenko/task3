﻿struct PaymentDetails
{
    public string vehicleId;        // номер ТЗ
    public float amountspent;       // списана сума
    public string vehicletype;      // тип ТЗ
    public float vehicle_bal;       // баланс транспортного засобу
}