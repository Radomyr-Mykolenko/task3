﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;


namespace Mainprog
{
  
    class MainMenu
    {  
        string[] MainMenuContainer = new string[]
            {
                /*1*/        "Дiзнатися поточний баланс парковки",
                /*2*/        "Сума зароблених коштiв за останню хвилину",
                /*3*/        "Дiзнатися кiлькiсть вiльних/зайнятих мiсць на парковцi",
                /*4*/        "Вивести на екран всi Транзакцii Парковки за останню хвилину",
                /*5*/        "Вивести усю iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log)",
                /*6*/        "Вивести на екран список усiх Транспортних засобiв на парковцi",
                /*7*/        "Створити/поставити Транспортний засiб на Парковку",
                /*8*/        "Видалити/забрати Транспортний засiб з Парковки",
                /*9*/        "Поповнити баланс конкретного Транспортного засобу"
            };

        public void ShowMainMenu()
        {
            int i = 1;
            foreach (string c in MainMenuContainer)
            {
                Console.WriteLine("  " + i++ + " - " + c + "\n");
            }
          
            Console.WriteLine();
        }

        public void ShowHeader()
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkGray;           
            Console.WriteLine("     ******************************************");
            Console.Write("     *    Програма ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("<< Парковка бiля дому >>");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("   *");
            Console.WriteLine("     ******************************************");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            
        }

        public void Execute_Task1()          // Дiзнатися поточний баланс парковки
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[0]}"); 
            Console.WriteLine();           
            Console.ReadKey();
            
        }

        public void Execute_Task2()                         //   "Сума зароблених коштiв за останню хвилину"
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[1]}");
            Console.WriteLine();                  
            Console.ReadKey();
        }

        public void Execute_Task3()          //  Дiзнатися кiлькiсть вільних/зайнятих мiсць на парковцi
        { 
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[2]}");
            Console.WriteLine();           
            Console.ReadKey();
        }

        public void Execute_Task4()                         //  Вивести на екран всi Транзакцii Парковки за останню хвилину
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[3]}");
            Console.WriteLine();
           
            Console.ReadKey();
            
        }

        public void Execute_Task5()                         //  Вивести усю iсторiю Транзакцiй (зчитавши данi з файлу Transactions.log)
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[4]}");
            Console.WriteLine();         
            Console.ReadKey();
        }

        public void Execute_Task6()                         // Вивести на екран список усiх Транспортних засобiв на парковцi
        { 
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[5]}");
            Console.WriteLine();   
            Console.ReadKey();
        }

        public void Execute_Task7()                         //  Створити/поставити Транспортний засiб на Парковку
        {
            Console.Clear();
            ShowHeader();           
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[6]}");
            Console.WriteLine();         
            Console.ReadKey();           
        }
      
        public void Execute_Task8()                         //  Видалити/забрати Транспортний засiб з Парковки
        {
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[7]}");
            Console.WriteLine();        
            Console.ReadKey();
        }

        public void Execute_Task9()                         //  Поповнити баланс конкретного Транспортного засобу
        { 
            Console.Clear();
            ShowHeader();
            Console.WriteLine($"  Ви вибрали завдання - {MainMenuContainer[8]}");
            Console.WriteLine();           
            Console.ReadKey();
        }
                   
        public void ExecuteMainMenu()
        {
            string n = "o";

            while (n != "q")
            {
                ShowHeader();
                ShowMainMenu();
                Console.WriteLine("  Виберiть необхiдний пункт меню або натиснiть q для виходу");
                n = Convert.ToString(Console.ReadLine());
                switch (n)
                {
                    case "1":
                        {
                            Execute_Task1();                          
                            break;
                        }
                    case "2":
                        {
                            Execute_Task2();
                            break;
                        }
                    case "3":
                        {
                            Execute_Task3();
                            break;
                        }
                    case "4":
                        {
                            Execute_Task4();
                            break;
                        }
                    case "5":
                        {
                            Execute_Task5();
                            break;
                        }
                    case "6":
                        {
                            Execute_Task6();
                            break;
                        }
                    case "7":
                        {
                            Execute_Task7();
                            break;
                        }
                    case "8":
                        {
                            Execute_Task8();
                            break;
                        }
                    case "9":
                        {
                            Execute_Task9();
                            break;
                        }
                    default:
                        {
                            
                            break;
                        }

                }
                Console.Clear();
            }
        }
    }

   

    class Program
    {
      
        static void Main(string[] args)
        {
                                             
            MainMenu mainmenu = new MainMenu();           
            mainmenu.ExecuteMainMenu();                                       
        }                              
       
    }

}
